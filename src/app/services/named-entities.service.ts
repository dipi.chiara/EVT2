import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { mapToArray } from '../utils/jsUtils';
import { NamedEntity } from '../models/evt-models';
import { Person, ListPerson, ListPlace, Place, PersName } from '../models/tei-model';
import { isNestedInElem } from '../utils/domUtils';

function sortPersons(p1: Person, p2: Person) {
  return p1.persName.forename.localeCompare(p2.persName.forename);
}

function sortPlaces(p1: Place, p2: Place) {
  return p1.id.localeCompare(p2.id);
}

@Injectable({
  providedIn: 'root'
})
export class NamedEntitiesService {
  private readonly personsEntities = new BehaviorSubject<ListPerson>({
    id: '',
    persons: {},
    relations: {}
  });
  readonly persons = this.personsEntities.pipe(map((p) => mapToArray(p.persons)));
  readonly relations = this.personsEntities.pipe(map((p) => p.relations));

  private readonly placesEntities = new BehaviorSubject<ListPlace>({
    id: '',
    places: {}
  });
  readonly places = this.placesEntities.pipe(map((p) => mapToArray(p.places)));

  // PERSONS
  initPersons(document: HTMLElement) {
    if (document) {
      const lists = document.querySelectorAll('listPerson');
      const parsedLists: ListPerson[] = [];
      [].forEach.call(lists, function (list: HTMLElement) {
        // We consider only first level lists; inset lists will be considered differently
        if (!isNestedInElem(list, list.tagName)) {
          const parsedList: ListPerson = {
            id: list.getAttribute('xml:id') || 'listPerson_' + parsedLists.length + 1 || undefined,
            persons: {},
            relations: {}
          };
          [].forEach.call(list.childNodes, function (child: HTMLElement) {
            if (child.nodeType === 1) {
              if (child.tagName === 'person') {
                const elId = child.getAttribute('xml:id') || 'person_' + mapToArray(parsedList.persons).length + 1;
                const forenameElements = child.getElementsByTagName('forename');
                const surnameElements = child.getElementsByTagName('surname');
                const person: Person = {
                  id: elId,
                  xml: child.outerHTML.replace(/ ?xmlns=["'].+["'] ?/gi, ''),
                  persName: {
                    forename: forenameElements && forenameElements[0] ? forenameElements[0].textContent : '',
                    surname: surnameElements && surnameElements[0] ? surnameElements[0].textContent : ''
                  }
                };
                [].forEach.call(child.childNodes, function (subchild: HTMLElement) {
                  if (subchild.nodeType === 1 && subchild.tagName && subchild.tagName !== 'persName') {
                    if (!person[subchild.tagName]) {
                      person[subchild.tagName] = [];
                    }
                    person[subchild.tagName].push(subchild);
                  }
                });
                parsedList.persons[elId] = person;
              } else if (child.tagName === 'listPerson') {
                // TODO: Parse Direct Sub list
              }
            }
          });
          parsedLists.push(parsedList);
        }
      });
      this.personsEntities.next(parsedLists[0]);
    }
  }

  getPersonEntity(id: string) {
    return this.personToEntity(this.personsEntities.value.persons[id]);
  }

  getPersons() {
    return this.persons.pipe(map((p) => p.sort(sortPersons)));
  }

  getPersonsAtGroupingKey(key: string) {
    return this.persons
      .pipe(
        map((p) => p.filter((item) => item.id[0].toLowerCase() === key.toLowerCase())),
        map((p) => p.sort(sortPersons)));
  }

  private personToEntity(person: Person): NamedEntity {
    const personEntity = {
      header: person.persName.forename + (person.persName.surname ? ' ' + person.persName.surname : ''),
      xml: person.xml,
      occurrences: [],
      info: [],
      type: 'person'
    };
    const personKeys = Object.keys(person);
    for (const key of personKeys) {
      let personInfo;
      if (['xml', 'id', 'persName', 'relations'].indexOf(key) < 0) {
        for (const personData of person[key]) {
          personInfo = {
            icon: key,
            text: personData.textContent,
            label: ''
          };
          for (const attrib of personData.attributes) {
            if (attrib.specified) {
              personInfo.label += attrib.value;
            }
          }
          personEntity.info.push(personInfo);
        }
      } else if (key === 'persName') {
        const persName: PersName = person[key];
        personInfo = {
          icon: key,
          text: persName.surname ? persName.forename + ' ' + persName.surname : persName.forename
        };
        personEntity.info.push(personInfo);
      }
    }
    return personEntity;
  }

  // PLACES
  initPlaces(document: HTMLElement) {
    if (document) {
      const lists = document.querySelectorAll('listPlace');
      const parsedLists: ListPlace[] = [];
      [].forEach.call(lists, function (list: HTMLElement) {
        // We consider only first level lists; inset lists will be considered differently
        if (!isNestedInElem(list, list.tagName)) {
          const parsedList: ListPlace = {
            id: list.getAttribute('xml:id') || 'listPlace_' + parsedLists.length + 1 || undefined,
            places: {}
          };
          [].forEach.call(list.childNodes, function (child: HTMLElement) {
            if (child.nodeType === 1) {
              if (child.tagName === 'place') {
                const elId = child.getAttribute('xml:id') || 'place_' + mapToArray(parsedList.places).length + 1;
                const place: Place = {
                  id: elId,
                  xml: child.outerHTML.replace(/ ?xmlns=["'].+["'] ?/gi, '')
                };
                [].forEach.call(child.childNodes, function (subchild: HTMLElement) {
                  if (subchild.nodeType === 1 && subchild.tagName) {
                    if (!place[subchild.tagName]) {
                      place[subchild.tagName] = [];
                    }
                    place[subchild.tagName].push(subchild);
                  }
                });
                parsedList.places[elId] = place;
              } else if (child.tagName === 'listPlace') {
                // TODO: Parse Direct Sub list
              }
            }
          });
          parsedLists.push(parsedList);
        }
      });
      this.placesEntities.next(parsedLists[0]);
    }
  }

  getPlaceEntity(id: string) {
    console.log(this.placesEntities.value);
    return this.placeToEntity(this.placesEntities.value.places[id]);
  }

  getPlaces() {
    return this.places.pipe(map((p) => p.sort(sortPlaces)));
  }

  getPlacesAtGroupingKey(key: string) {
    return this.places
      .pipe(
        map((p) => p.filter((item) => item.id[0].toLowerCase() === key.toLowerCase())),
        map((p) => p.sort(sortPlaces)));
  }

  private placeToEntity(place: Place): NamedEntity {
    console.log(place);
    const placeEntity = {
      header: place.placeName ? place.placeName[0].textContent : place.id,
      xml: place.xml,
      occurrences: [],
      info: [],
      type: 'place'
    };

    const placeKeys = Object.keys(place);
    for (const key of placeKeys) {
      if (['xml', 'id'].indexOf(key) < 0) {
        for (const placeData of place[key]) {
          const placeInfo = {
            icon: key,
            text: placeData.textContent,
            label: ''
          };
          for (const attrib of placeData.attributes) {
            if (attrib.specified) {
              placeInfo.label += attrib.value;
            }
          }
          placeEntity.info.push(placeInfo);
        }
      }
    }
    return placeEntity;
  }
}
