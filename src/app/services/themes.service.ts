import { Injectable } from '@angular/core';

@Injectable()
export class ThemesService {
    themes: ColorTheme[];
    currentTheme: ColorTheme;

    constructor() {
        this.themes = [{
            value: 'grey',
            label: 'Grey'
        }, {
            value: 'blue',
            label: 'Blue'
        }, {
            value: 'brown',
            label: 'Brown'
        }];
        this.currentTheme = this.themes[1];
    }

    selectTheme(theme: ColorTheme) {
        this.currentTheme = theme;
    }

    getAvailableThemes(): ColorTheme[] {
        return this.themes;
    }

    getCurrentTheme(): ColorTheme {
        return this.currentTheme;
    }

    getCurrentThemeValue(): string {
        return this.currentTheme.value;
    }
}

export interface ColorTheme {
  value: string;
  label: string;
}
