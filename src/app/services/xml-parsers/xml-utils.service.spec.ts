import { TestBed, inject } from '@angular/core/testing';

import { XmlUtilsService } from './xml-utils.service';

describe('XmlUtilsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [XmlUtilsService]
    });
  });

  it('should be created', inject([XmlUtilsService], (service: XmlUtilsService) => {
    expect(service).toBeTruthy();
  }));
});
