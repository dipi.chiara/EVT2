import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import { getElementsBetweenTree } from '../../utils/domUtils';
import { PageData, EditionStructure } from '../../models/evt-models';

import { Map } from '../../utils/jsUtils';

@Injectable()
export class StructureXmlParserService {
  private readonly editionStructure = new BehaviorSubject<EditionStructure>({
    pages: {},
    pagesIndexes: []
  });

  getStructure(): Observable<EditionStructure> {
    return this.editionStructure;
  }

  init(document: HTMLElement) {
    const pages: Map<PageData> = {};
    const pagesIndexes: string[] = [];
    const pageTagName = 'pb';

    if (document) {
      // console.time('getStructure');
      const pageElements = document.querySelectorAll(pageTagName);
      const l = pageElements.length;
      for (let i = 0; i < l; i++) {
        const element = pageElements[i];
        let pageContent: any[] = [];
        if (i < l - 1) { // TODO: handle last page
          pageContent = getElementsBetweenTree(element, pageElements[i + 1]);
        }
        const page: PageData = {
          id: element.getAttribute('xml:id') || 'page_' + (pagesIndexes.length + 1),
          label: element.getAttribute('n') || 'Page ' + (pagesIndexes.length + 1),
          xmlSource: element,
          content: pageContent
        };
        pages[page.id] = page;
        pagesIndexes.push(page.id);
      }
      // console.timeEnd('getStructure');
      console.log(pages);
    }
    this.editionStructure.next({
      pages: pages,
      pagesIndexes: pagesIndexes
    });
  }

}
