import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';

import { TextComponent } from '../../components/text/text.component';
import { GenericElementComponent } from '../../components/generic-element/generic-element.component';
import { NoteComponent } from '../../components/note/note.component';
import { NamedEntityRefComponent } from '../../components/named-entity/named-entity-ref/named-entity-ref.component';
import { xpath } from '../../utils/domUtils';
import { NamedEntityRefData, TextData, GenericElementData, NoteData, CommentData } from '../../models/parsable-elements';

type ParsableElement = HTMLElement | TextData | GenericElementData | CommentData | NamedEntityRefData | NoteData;

@Injectable()
export class GenericParserService {
  private readonly editionSource = new BehaviorSubject<string>(undefined);

  constructor(private http: HttpClient) { }

  loadSource(url: string) {
    this.http.get(url, { responseType: 'text' })
      .pipe(tap((source: string) => this.editionSource.next(source)));
  }
  parseText(xml: HTMLElement): Promise<TextData> {
    const text = { type: TextComponent, text: xml.textContent } as TextData;
    return Promise.resolve(text);
  }

  parseElement(xml: HTMLElement): Promise<GenericElementData> {
    const genericElement: GenericElementData = {
      type: GenericElementComponent,
      class: xml.tagName,
      content: xml.childNodes
    };
    return Promise.resolve(genericElement);
  }

  parseNote(xml: HTMLElement): Promise<NoteData> {
    const noteElement: NoteData = {
      type: NoteComponent,
      path: xpath(xml),
      content: xml.childNodes
    };
    return Promise.resolve(noteElement);
  }

  parseEntityRef(xml: HTMLElement): Promise<NamedEntityRefData> {
    const path = xpath(xml);
    const ref = xml.getAttribute('ref');
    const namedEntityElement = {
      type: NamedEntityRefComponent,
      path: path,
      entityType: xml.tagName,
      content: xml.childNodes,
      entityId: ref ? ref.replace(/#/g, '') : xpath
    } as NamedEntityRefData;
    return Promise.resolve(namedEntityElement);
  }

  parse(xml: HTMLElement): Promise<ParsableElement> {
    if (xml) {
      if (xml.nodeType === 3) {  // Text
        return this.parseText(xml);
      }
      if (xml.nodeType === 8) { // Comment
        return Promise.resolve({ type: 'comment' } as CommentData);
      }

      switch (xml.tagName) {
        // case 'rdg':
        // 	return this.parseRdg(xml);
        // case 'body':
        // 	return this.parseBody(xml)
        case 'note':
          return this.parseNote(xml);
        case 'placeName':
        case 'persName':
        case 'orgName':
          return this.parseEntityRef(xml);
        default:
          return this.parseElement(xml);
      }
    } else {
      return Promise.resolve(xml);
    }
  }
}
