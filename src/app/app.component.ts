import { Component } from '@angular/core';
import { Language } from 'angular-l10n';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import { ConfigurationService } from './configuration/configuration.service';
import { StructureXmlParserService } from './services/xml-parsers/structure-xml-parser.service';
import { NamedEntitiesService } from './services/named-entities.service';
import { FileConfig } from './configuration/evt-config';
import { XmlUtilsService } from './services/xml-parsers/xml-utils.service';

@Component({
    selector: 'evt-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    @Language() lang: string;
    private editionSource = new BehaviorSubject<HTMLElement>(undefined);

    public modalShown = '';

    constructor(
        private http: HttpClient,
        private configuration: ConfigurationService,
        private structureXML: StructureXmlParserService,
        private namedEntities: NamedEntitiesService,
        private xmlUtils: XmlUtilsService) {
        this.configuration.init();
        this.init();
    }

    private init() {
        this.configuration.getFileConfig()
            .subscribe((fileConfig: FileConfig) => {
                if (fileConfig && fileConfig.editionUrls.length > 0) {
                    this.http.get(fileConfig.editionUrls[0], { responseType: 'text' })
                        .subscribe((source) => {
                            let document: HTMLElement;
                            if (typeof source !== 'object' && typeof source === 'string') {
                                document = this.xmlUtils.parseXml(source);
                            } else {
                                document = source;
                            }
                            this.editionSource.next(document);
                        });
                }
            });

        this.editionSource.subscribe((source: HTMLElement) => {
            this.namedEntities.initPersons(source);
            this.namedEntities.initPlaces(source);
            this.structureXML.init(source);
        });
    }


    toggleModal(modalToShow) {
        this.modalShown = modalToShow && this.modalShown !== modalToShow ? modalToShow : undefined;
    }

    getBookmark() {
        return window.location.href;
    }
}
