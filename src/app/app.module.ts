import { BrowserModule } from '@angular/platform-browser';
import { GridsterModule } from 'angular-gridster2';
import { DynamicModule } from 'ng-dynamic-component';

import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { L10nConfig, L10nLoader, TranslationModule, StorageStrategy, ProviderType } from 'angular-l10n';
import { RouterModule, Routes } from '@angular/router';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2HandySyntaxHighlighterModule } from 'ng2-handy-syntax-highlighter';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faInfo, faTimes, faEllipsisV, faInfoCircle, faClipboardList, faBookmark, faDownload,
    faLanguage, faPaintBrush, faKeyboard, faThumbtack, faPenSquare, faBarcode, faStickyNote,
    faBriefcase, faUser, faVenusMars, faBirthdayCake, faHome, faUsers, faMapMarker,
    faLocationArrow, faShareAlt, faSpinner, faCaretDown, faCaretRight
} from '@fortawesome/free-solid-svg-icons';

import { AppComponent } from './app.component';

import { MainHeaderComponent } from './components/generic-ui/main-header/main-header.component';

import { ConfigurationService } from './configuration/configuration.service';
import { ThemesService } from './services/themes.service';
import { XmlUtilsService } from './services/xml-parsers/xml-utils.service';
import { StructureXmlParserService } from './services/xml-parsers/structure-xml-parser.service';
import { NamedEntitiesService } from './services/named-entities.service';
import { GenericParserService } from './services/xml-parsers/generic-parser.service';

import { DocumentComponent } from './components/document/document.component';
import { PageComponent } from './components/page/page.component';
import { TextComponent } from './components/text/text.component';
import { GenericElementComponent } from './components/generic-element/generic-element.component';
import { ContentViewerComponent } from './components/content-viewer/content-viewer.component';
import { NoteComponent } from './components/note/note.component';
import { NamedEntityComponent } from './components/named-entity/named-entity/named-entity.component';
import { NamedEntityRefComponent } from './components/named-entity/named-entity-ref/named-entity-ref.component';
import { PaneComponent } from './components/generic-ui/pane/pane.component';
import { TextPaneComponent } from './components/generic-ui/pane/text-pane/text-pane.component';
import { ImagePaneComponent } from './components/generic-ui/pane/image-pane/image-pane.component';
import { ReadingTextComponent } from './components/view-modes/reading-text/reading-text.component';
import { ImageTextComponent } from './components/view-modes/image-text/image-text.component';
import { MainMenuComponent } from './components/generic-ui/main-menu/main-menu.component';
import { ModalComponent } from './components/generic-ui/modal/modal.component';
import { TextTextComponent } from './components/view-modes/text-text/text-text.component';
import { CollationComponent } from './components/view-modes/collation/collation.component';
import { WitnessPaneComponent } from './components/generic-ui/pane/witness-pane/witness-pane.component';
import { TextSourcesComponent } from './components/view-modes/text-sources/text-sources.component';
import { TextVersionsComponent } from './components/view-modes/text-versions/text-versions.component';
import { VersionPaneComponent } from './components/generic-ui/pane/version-pane/version-pane.component';
import { SourcesPaneComponent } from './components/generic-ui/pane/sources-pane/sources-pane.component';
import { NamedEntitiesListComponent } from './components/named-entity/named-entities-list/named-entities-list.component';
import { NamedEntityDetailComponent } from './components/named-entity/named-entity/named-entity-detail/named-entity-detail.component';
import { XmlBeautifyPipe } from './pipes/xml-beautify.pipe';
import { PageSelectorComponent } from './components/generic-ui/selectors/page-selector/page-selector.component';

library.add(faEllipsisV);
library.add(faInfo);
library.add(faTimes);
library.add(faInfoCircle);
library.add(faClipboardList);
library.add(faBookmark);
library.add(faDownload);
library.add(faLanguage);
library.add(faPaintBrush);
library.add(faKeyboard);
library.add(faThumbtack);
library.add(faPenSquare);

library.add(faBarcode);
library.add(faStickyNote);
library.add(faBriefcase);
library.add(faUser);
library.add(faVenusMars);
library.add(faBirthdayCake);
library.add(faHome);
library.add(faUsers);
library.add(faMapMarker);
library.add(faLocationArrow);
library.add(faShareAlt);
library.add(faSpinner);
library.add(faCaretDown);
library.add(faCaretRight);

const l10nConfig: L10nConfig = {
    locale: {
        languages: [
            { code: 'en', dir: 'ltr' },
            { code: 'it', dir: 'ltr' }
        ],
        language: 'en',
        storage: StorageStrategy.Cookie
    },
    translation: {
        providers: [
            { type: ProviderType.Static, prefix: './assets/i10n/locale-' }
        ],
        caching: true,
        missingValue: 'No key'
    }
};

const appRoutes: Routes = [
    { path: '', redirectTo: '/readingText', pathMatch: 'full' },
    { path: 'imageText', component: ImageTextComponent },
    { path: 'imageText/:doc/:page', component: ImageTextComponent },
    { path: 'readingText', component: ReadingTextComponent },
    { path: 'readingText/:doc/:page', component: ReadingTextComponent },
    { path: 'textText', component: TextTextComponent },
    { path: 'textText/:doc/:page', component: TextTextComponent },
    { path: 'collation', component: CollationComponent },
    { path: 'collation/:doc/:page', component: CollationComponent },
    { path: 'textSources', component: TextSourcesComponent },
    { path: 'textSources/:doc/:page', component: TextSourcesComponent },
    { path: 'textVersions', component: TextVersionsComponent },
    { path: 'textVersions/:doc/:page', component: TextVersionsComponent }
];

@NgModule({
    declarations: [
        AppComponent,
        MainHeaderComponent,
        DocumentComponent,
        PageComponent,
        TextComponent,
        GenericElementComponent,
        ContentViewerComponent,
        NoteComponent,
        NamedEntityComponent,
        NamedEntityRefComponent,
        PaneComponent,
        TextPaneComponent,
        ImagePaneComponent,
        ReadingTextComponent,
        ImageTextComponent,
        MainMenuComponent,
        ModalComponent,
        TextTextComponent,
        CollationComponent,
        WitnessPaneComponent,
        TextSourcesComponent,
        TextVersionsComponent,
        VersionPaneComponent,
        SourcesPaneComponent,
        NamedEntitiesListComponent,
        NamedEntityDetailComponent,
        XmlBeautifyPipe,
        PageSelectorComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        TranslationModule.forRoot(l10nConfig),
        RouterModule.forRoot(
            appRoutes,
            { enableTracing: false } // <-- debugging purposes only
        ),
        InfiniteScrollModule,
        NgbModule.forRoot(),
        FontAwesomeModule,
        Ng2HandySyntaxHighlighterModule,
        GridsterModule,
        DynamicModule.withComponents([
            TextComponent,
            GenericElementComponent,
            NoteComponent,
            NamedEntityRefComponent])
    ],
    providers: [
        ConfigurationService,
        GenericParserService,
        NamedEntitiesService,
        StructureXmlParserService,
        ThemesService,
        XmlUtilsService,
    ],
    bootstrap: [AppComponent],
    entryComponents: [

    ]
})
export class AppModule {
    constructor(public l10nLoader: L10nLoader) {
        this.l10nLoader.load();
    }
}
