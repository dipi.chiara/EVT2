import * as vkbeautify from 'vkbeautify';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'xmlBeautify'
})
export class XmlBeautifyPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return vkbeautify.xml(value);
  }

}
