export interface EVTConfig {
  uiConfig: UiConfig;
  editionConfig: EditionConfig;
  fileConfig: FileConfig;
}

export interface UiConfig {
  localization: boolean;
}

export interface EditionConfig {
  title: string;
}

export interface FileConfig {
  editionUrls: string[];
}
