import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { UiConfig, EditionConfig, FileConfig } from './evt-config';

@Injectable()
export class ConfigurationService {
  private uiConfig = new BehaviorSubject<UiConfig>(undefined);
  private fileConfig = new BehaviorSubject<FileConfig>(undefined);
  private editionConfig = new BehaviorSubject<EditionConfig>(undefined);

  private uiConfigUrl = 'assets/config/ui_config.json';
  private fileConfigUrl = 'assets/config/file_config.json';
  private editionConfigUrl = 'assets/config/edition_config.json';

  constructor(private http: HttpClient) {
  }

  init() {
    this.initUiConfig();
    this.initFileConfig();
    this.initEditionConfig();
  }

  getUiConfig(): Observable<UiConfig> {
    return this.uiConfig;
  }

  getFileConfig(): Observable<FileConfig> {
    return this.fileConfig;
  }

  getEditionConfig(): Observable<EditionConfig> {
    return this.editionConfig;
  }

  private initUiConfig() {
    this.http.get<UiConfig>(this.uiConfigUrl)
      .subscribe((uiConfig: UiConfig) => this.uiConfig.next(uiConfig));
  }

  private initFileConfig() {
    this.http.get<FileConfig>(this.fileConfigUrl)
      .subscribe((fileConfig: FileConfig) => this.fileConfig.next(fileConfig));
  }

  private initEditionConfig() {
    this.http.get<EditionConfig>(this.editionConfigUrl)
      .subscribe((editionConfig: EditionConfig) => this.editionConfig.next(editionConfig));
  }
}
