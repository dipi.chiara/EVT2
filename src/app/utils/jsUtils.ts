export interface Map<T> {
    [key: string]: T;
}

export function mapToArray<T>(m: Map<T>) {
    return Object.keys(m).map((id) => m[id]);
}
