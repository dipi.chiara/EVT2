import { Component, Input } from '@angular/core';

import { PageData } from '../../models/evt-models';

@Component({
    selector: 'evt-document',
    templateUrl: './document.component.html',
    styleUrls: ['./document.component.scss'],
})
export class DocumentComponent {
    @Input() page: PageData;
}
