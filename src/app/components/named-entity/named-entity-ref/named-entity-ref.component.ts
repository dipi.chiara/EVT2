import { NamedEntitiesService } from './../../../services/named-entities.service';
import { Component, Input } from '@angular/core';
import { NamedEntity } from '../../../models/evt-models';
import { NamedEntityRefData } from '../../../models/parsable-elements';

@Component({
  selector: 'evt-named-entity-ref',
  templateUrl: './named-entity-ref.component.html',
  styleUrls: ['./named-entity-ref.component.scss']
})
export class NamedEntityRefComponent {
  @Input() data: NamedEntityRefData;

  entity: NamedEntity;

  public visible = false;

  constructor(
    readonly namedEntities: NamedEntitiesService
  ) {
  }

  goToEntityInList() {
    if (!!this.entity) {
      this.entity = undefined;
    } else {
      if (this.data.entityType === 'persName') {
        this.entity = this.namedEntities.getPersonEntity(this.data.entityId);
      } else {
        this.entity = this.namedEntities.getPlaceEntity(this.data.entityId);
      }
    }
  }
}
