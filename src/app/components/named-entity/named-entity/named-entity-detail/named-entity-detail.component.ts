import { Component, Input } from '@angular/core';

@Component({
  selector: 'evt-named-entity-detail',
  templateUrl: './named-entity-detail.component.html',
  styleUrls: ['./named-entity-detail.component.scss']
})
export class NamedEntityDetailComponent {
  @Input() icon: string;
  @Input() text: string;
  @Input() label: string;

}
