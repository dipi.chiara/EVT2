import { Component, Input, OnInit } from '@angular/core';
import { NamedEntity } from '../../../models/evt-models';
import { Language } from 'angular-l10n';

@Component({
  selector: 'evt-named-entity',
  templateUrl: './named-entity.component.html',
  styleUrls: ['./named-entity.component.scss']
})
export class NamedEntityComponent implements OnInit {
  @Language() lang: string;
  @Input() entity: NamedEntity;
  @Input() inList: boolean;
  public contentOpened = true;

  ngOnInit() {
    if (this.inList) {
      this.contentOpened = false;
    }
  }

  toggleContent() {
    this.contentOpened = !this.contentOpened;
  }
}
