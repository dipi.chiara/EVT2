import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Language } from 'angular-l10n';

import { NamedEntitiesService } from './../../../services/named-entities.service';
import { Person } from '../../../models/tei-model';

@Component({
  selector: 'evt-named-entities-list',
  templateUrl: './named-entities-list.component.html',
  styleUrls: ['./named-entities-list.component.scss']
})
export class NamedEntitiesListComponent {
  @Language() lang: string;
  public selectedKey: string;
  public navigationKeys: string[];
  public persons$: Observable<Person[]>;
  private totPersons: Person[] = [];
  private totPersonsLoaded = 10;

  constructor(private namedEntities: NamedEntitiesService) {
    this.navigationKeys = this.genCharArray('a', 'z');
    this.selectedKey = this.navigationKeys[0];
    this.persons$ = this.namedEntities.getPersonsAtGroupingKey(this.selectedKey);
    this.persons$.subscribe((p) => this.totPersons = p);
  }

  getVisiblePersons() {
    return this.totPersons.slice(0, this.totPersonsLoaded);
  }
  loadMoreElements() {
    if (this.totPersonsLoaded + 10 <= this.totPersons.length - 1) {
      this.totPersonsLoaded += 10;
    } else {
      this.totPersonsLoaded = this.totPersons.length - 1;
    }
  }

  selectKey(key: string) {
    this.selectedKey = key;
    this.totPersonsLoaded = 10;
    this.totPersons = [];
    this.persons$ = this.namedEntities.getPersonsAtGroupingKey(key);
    this.persons$.subscribe((p) => this.totPersons = p);
  }

  // TEMP
  private genCharArray(charA, charZ) {
    const a = [], j = charZ.charCodeAt(0);
    let i = charA.charCodeAt(0);
    for (; i <= j; ++i) {
      a.push(String.fromCharCode(i));
    }
    return a;
  }
}
