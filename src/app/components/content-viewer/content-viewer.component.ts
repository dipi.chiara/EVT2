import { Component, OnInit, OnDestroy, Input, ViewChild, ViewContainerRef, ComponentRef } from '@angular/core';

import { GenericParserService } from '../../services/xml-parsers/generic-parser.service';

@Component({
  selector: 'evt-content-viewer',
  templateUrl: './content-viewer.component.html'
})
export class ContentViewerComponent implements OnInit, OnDestroy {
  @Input() content: HTMLElement;
  @ViewChild('container', { read: ViewContainerRef }) container: ViewContainerRef;
  private componentRef: ComponentRef<{}>;

  public parsedContent: any;
  public inputs: { [keyName: string]: any };
  public outputs: { [keyName: string]: Function };

  constructor(private parser: GenericParserService) {
  }

  ngOnInit() {
    this.parser.parse(this.content)
      .then(
        (parsedContent: any) => {
          this.parsedContent = parsedContent;
          this.inputs = { 'data': parsedContent };
          this.outputs = {};
        },
        (err) => console.error(err));
  }

  ngOnDestroy() {
    if (this.componentRef) {
      this.componentRef.destroy();
      this.componentRef = null;
    }
  }
}
