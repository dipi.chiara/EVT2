import { Component, Input } from '@angular/core';

import { ThemesService } from '../../services/themes.service';
import { PageData } from '../../models/evt-models';

@Component({
    selector: 'evt-page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.scss']
})
export class PageComponent {
    @Input() data: PageData;

    constructor(public themes: ThemesService) {
    }
}
