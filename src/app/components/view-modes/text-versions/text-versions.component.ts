import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GridsterConfig, GridsterItem, GridType, DisplayGrid, CompactType } from 'angular-gridster2';

import { PageData, EditionStructure } from '../../../models/evt-models';
import { StructureXmlParserService } from '../../../services/xml-parsers/structure-xml-parser.service';

@Component({
  selector: 'evt-text-versions',
  templateUrl: './text-versions.component.html',
  styleUrls: ['./text-versions.component.scss']
})
export class TextVersionsComponent implements OnInit {
  @ViewChild('versionsPane') versionsPane: ElementRef;
  public page: PageData;
  private versions: VersionItem[] = [];

  public options: GridsterConfig = {};
  public textPaneItem: GridsterItem = { cols: 1, rows: 1, y: 0, x: 0 };
  public versionsPaneItem: GridsterItem = { cols: 1, rows: 1, y: 0, x: 1 };
  public versionsOptions: GridsterConfig = {};

  constructor(
    private route: ActivatedRoute,
    private editionStructure: StructureXmlParserService) { }

  ngOnInit() {
    this.initGridster();
    this.initPageAndVersions();
  }

  getVersions() {
    return this.versions;
  }

  addVersion() {
    const newVersion = {
      label: (this.versions.length + 1).toString(), // TEMP
      itemConfig: { cols: 1, rows: 1, y: 0, x: this.versions.length + 1 }
    };
    this.versions.push(newVersion); // TEMP
    this.updateGridsterOptions();
    // TODO: Come gestiamo la rotta nel caso di più versioni selezionate?
  }

  removeVersion(index) {
    this.versions.splice(index, 1);
    this.updateGridsterOptions();
  }

  private initPageAndVersions() {
    this.route.params
      .subscribe((params) => {
        this.editionStructure.getStructure().subscribe((editionStructure: EditionStructure) => {
          if (params.page && editionStructure.pages[params.page]) {
            this.page = editionStructure.pages[params.page];
          } else {
            this.page = editionStructure.pages[editionStructure.pagesIndexes[0]];
          }
        });
        this.versions = params.ws ? params.ws.split(',') : [];
      });
  }

  private initGridster() {
    this.options = {
      gridType: GridType.Fit,
      displayGrid: DisplayGrid.None,
      margin: 0,
      maxCols: 2,
      maxRows: 1,
      draggable: {
        enabled: false
      },
      resizable: {
        enabled: false
      }
    };
    this.versionsOptions = {
      gridType: GridType.Fit,
      displayGrid: DisplayGrid.None,
      compactType: CompactType.CompactLeft,
      scrollToNewItems: true,
      margin: 0,
      maxRows: 1,
      draggable: {
        enabled: true
      },
      resizable: {
        enabled: false
      },
      mobileBreakpoint: 0,
      itemResizeCallback: this.updateFixedColWidth.bind(this),
      itemChangeCallback: this.itemChange.bind(this)
    };
  }

  private itemChange() {
    const updatedVerList: string[] = [];
    for (const verItem of this.versions) {
      const verIndex = verItem.itemConfig.x;
      updatedVerList[verIndex] = verItem.label;
    }
    // TODO: Use this list to update URL params
    console.log('TODO! Use this list to update URL params', updatedVerList);
  }

  private updateGridsterOptions() {
    this.options.maxCols = this.versions.length <= 1 ? 2 : 3;
    this.versionsPaneItem.cols = this.versions.length <= 1 ? 1 : 2;

    this.versionsOptions.maxCols = this.versions.length;

    this.versionsOptions.gridType = this.versions.length <= 2 ? GridType.Fit : GridType.HorizontalFixed;
    this.changedOptions();
    this.updateFixedColWidth();
  }

  private changedOptions() {
    if (this.options.api && this.options.api.optionsChanged) {
      this.options.api.optionsChanged();
    }
    if (this.versionsOptions.api && this.versionsOptions.api.optionsChanged) {
      this.versionsOptions.api.optionsChanged();
    }
  }

  private updateFixedColWidth() {
    const versionsPaneEl = <HTMLElement>this.versionsPane.nativeElement;
    const fixedColWidth = versionsPaneEl.clientWidth * 0.416666666667;
    this.versionsOptions.fixedColWidth = this.versions.length > 2 ? fixedColWidth : undefined;
    this.changedOptions();
  }
}
interface VersionItem {
  label: string;
  itemConfig: GridsterItem;
}
