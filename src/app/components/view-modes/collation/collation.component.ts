import { StructureXmlParserService } from './../../../services/xml-parsers/structure-xml-parser.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GridsterConfig, GridsterItem, GridType, DisplayGrid, CompactType } from 'angular-gridster2';

import { PageData, EditionStructure } from '../../../models/evt-models';

@Component({
  selector: 'evt-collation',
  templateUrl: './collation.component.html',
  styleUrls: ['./collation.component.scss']
})
export class CollationComponent implements OnInit {
  @ViewChild('collationPane') collationPane: ElementRef;

  public page: PageData;
  private witnesses: WitnessItem[] = [];

  public options: GridsterConfig = {};
  public textPaneItem: GridsterItem = { cols: 1, rows: 1, y: 0, x: 0 };
  public collationPaneItem: GridsterItem = { cols: 1, rows: 1, y: 0, x: 1 };
  public collationOptions: GridsterConfig = {};

  constructor(
    private route: ActivatedRoute,
    private editionStructure: StructureXmlParserService) { }

  ngOnInit() {
    this.initGridster();
    this.initPageAndWitnesses();
  }

  getWitnesses() {
    return this.witnesses;
  }

  addWitness() {
    const id = (this.witnesses.length + 1).toString(); // TEMP
    const newWit = {
      label: id,
      itemConfig: { cols: 1, rows: 1, y: 0, x: this.witnesses.length + 1, id: id }
    };

    this.witnesses.push(newWit); // TEMP
    this.updateGridsterOptions();
    // TODO: Come gestiamo la rotta nel caso di testimoni collazionati?
  }

  removeWitness(index) {
    this.witnesses.splice(index, 1);
    this.updateGridsterOptions();
  }

  private initPageAndWitnesses() {
    this.route.params
      .subscribe((params) => {
        this.editionStructure.getStructure().subscribe((editionStructure: EditionStructure) => {
          if (params.page && editionStructure.pages[params.page]) {
            this.page = editionStructure.pages[params.page];
          } else {
            this.page = editionStructure.pages[editionStructure.pagesIndexes[0]];
          }
        });
        this.witnesses = params.ws ? params.ws.split(',') : [];
      });
  }

  private initGridster() {
    this.options = {
      gridType: GridType.Fit,
      displayGrid: DisplayGrid.None,
      margin: 0,
      maxCols: 2,
      maxRows: 1,
      draggable: {
        enabled: false
      },
      resizable: {
        enabled: false
      }
    };
    this.collationOptions = {
      gridType: GridType.Fit,
      displayGrid: DisplayGrid.None,
      compactType: CompactType.CompactLeft,
      scrollToNewItems: true,
      margin: 0,
      maxRows: 1,
      draggable: {
        enabled: true
      },
      resizable: {
        enabled: false
      },
      mobileBreakpoint: 0,
      itemResizeCallback: this.updateFixedColWidth.bind(this),
      itemChangeCallback: this.itemChange.bind(this)
    };
  }

  private itemChange() {
    const updatedWitList: string[] = [];
    for (const witItem of this.witnesses) {
      const witIndex = witItem.itemConfig.x;
      updatedWitList[witIndex] = witItem.label;
    }
    // TODO: Use this list to update URL params
    console.log('TODO! Use this list to update URL params', updatedWitList);
  }

  private updateGridsterOptions() {
    this.options.maxCols = this.witnesses.length <= 1 ? 2 : 3;
    this.collationPaneItem.cols = this.witnesses.length <= 1 ? 1 : 2;

    this.collationOptions.maxCols = this.witnesses.length;
    this.collationOptions.gridType = this.witnesses.length <= 2 ? GridType.Fit : GridType.HorizontalFixed;
    this.changedOptions();
    this.updateFixedColWidth();
  }

  private changedOptions() {
    if (this.options.api && this.options.api.optionsChanged) {
      this.options.api.optionsChanged();
    }
    if (this.collationOptions.api && this.collationOptions.api.optionsChanged) {
      this.collationOptions.api.optionsChanged();
    }
  }

  private updateFixedColWidth() {
    const collationPaneEl = <HTMLElement>this.collationPane.nativeElement;
    const fixedColWidth = collationPaneEl.clientWidth * 0.416666666667;
    this.collationOptions.fixedColWidth = this.witnesses.length > 2 ? fixedColWidth : undefined;
    this.changedOptions();
  }
}

interface WitnessItem {
  label: string;
  itemConfig: GridsterItem;
}
