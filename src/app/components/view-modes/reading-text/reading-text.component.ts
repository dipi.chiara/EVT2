import { GridsterConfig, GridType, DisplayGrid, GridsterItem } from 'angular-gridster2';
import { StructureXmlParserService } from './../../../services/xml-parsers/structure-xml-parser.service';

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PageData, EditionStructure } from '../../../models/evt-models';

@Component({
    selector: 'evt-reading-text',
    templateUrl: './reading-text.component.html',
    styleUrls: ['./reading-text.component.scss']
})
export class ReadingTextComponent implements OnInit {
    public page: PageData;

    public pinnedBoardOpened = false;
    public apparatusesOpened = true;

    // Gridster CONFIGS
    public options: GridsterConfig = {};
    public textPaneItem: GridsterItem = { cols: 1, rows: 1, y: 0, x: 0 };
    public pinnedBoardItem: GridsterItem = { cols: 1, rows: 1, y: 0, x: 1 };
    public apparatusesItem: GridsterItem = { cols: 1, rows: 1, y: 0, x: 1 };

    constructor(
        private route: ActivatedRoute,
        private editionStructure: StructureXmlParserService) {
    }

    ngOnInit() {
        this.initGridster();
        this.initPage();
    }

    togglePinnedBoard() {
        this.pinnedBoardOpened = !this.pinnedBoardOpened;
        this.updateGridsterConfig();
    }

    toggleApparatuses() {
        this.updateGridsterConfig();
        this.apparatusesOpened = !this.apparatusesOpened;
    }
    changedOptions() {
        if (this.options.api && this.options.api.optionsChanged) {
            this.options.api.optionsChanged();
        }
    }
    private initPage() {
        this.route.params
            .subscribe((params) => {
                this.editionStructure.getStructure().subscribe(
                    (editionStructure: EditionStructure) => {
                        if (params.page && editionStructure.pages[params.page]) {
                            this.page = editionStructure.pages[params.page];
                        } else {
                            this.page = editionStructure.pages[editionStructure.pagesIndexes[0]];
                        }
                    });
            });
    }

    private updateGridsterConfig() {
        this.pinnedBoardItem.x = 1;
        this.apparatusesItem.x = this.pinnedBoardOpened ? 2 : (this.textPaneItem.x !== 0 ? 0 : 1);
        this.changedOptions();
    }


    private initGridster() {
        this.options = {
            gridType: GridType.Fit,
            displayGrid: DisplayGrid.None,
            margin: 0,
            maxCols: 3,
            maxRows: 1,
            draggable: {
                enabled: false
            },
            resizable: {
                enabled: false
            }
        };
    }
}
