import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PageData, EditionStructure } from './../../../models/evt-models';
import { StructureXmlParserService } from './../../../services/xml-parsers/structure-xml-parser.service';
import { GridsterConfig, GridsterItem, GridType, DisplayGrid } from 'angular-gridster2';

@Component({
    selector: 'evt-image-text',
    templateUrl: './image-text.component.html',
    styleUrls: ['./image-text.component.scss']
})
export class ImageTextComponent implements OnInit {
    public page: PageData;

    public options: GridsterConfig = {};
    public imagePaneItem: GridsterItem = { cols: 1, rows: 1, y: 0, x: 0 };
    public textPaneItem: GridsterItem = { cols: 1, rows: 1, y: 0, x: 1 };

    constructor(
        private route: ActivatedRoute,
        private editionStructure: StructureXmlParserService) { }

    ngOnInit() {
        this.initGridster();
        this.initPage();
    }

    private initPage() {
        this.route.params
            .subscribe((params) => {
                this.editionStructure.getStructure().subscribe((editionStructure: EditionStructure) => {
                    if (params.page && editionStructure.pages[params.page]) {
                        this.page = editionStructure.pages[params.page];
                    } else {
                        this.page = editionStructure.pages[editionStructure.pagesIndexes[0]];
                    }
                });
            });
    }

    private initGridster() {
        this.options = {
            gridType: GridType.Fit,
            displayGrid: DisplayGrid.None,
            margin: 0,
            maxCols: 2,
            maxRows: 1,
            draggable: {
                enabled: true
            },
            resizable: {
                enabled: false
            }
        };
    }

}
