import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PageData } from '../../../../models/evt-models';

@Component({
  selector: 'evt-version-pane',
  templateUrl: './version-pane.component.html',
  styleUrls: ['./version-pane.component.scss']
})
export class VersionPaneComponent {
  @Input() version: string;
  @Input() page: PageData;
  @Output() close = new EventEmitter<boolean>();

  emitClose() {
    this.close.emit(true);
  }
}
