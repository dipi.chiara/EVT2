import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VersionPaneComponent } from './version-pane.component';

describe('VersionPaneComponent', () => {
  let component: VersionPaneComponent;
  let fixture: ComponentFixture<VersionPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VersionPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
