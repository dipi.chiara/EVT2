import { Component } from '@angular/core';

@Component({
  selector: 'evt-sources-pane',
  templateUrl: './sources-pane.component.html',
  styleUrls: ['./sources-pane.component.scss']
})
export class SourcesPaneComponent {
  public source: string;

  selectSource(source: string) {
    this.source = source;
  }
}
