import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourcesPaneComponent } from './sources-pane.component';

describe('SourcesPaneComponent', () => {
  let component: SourcesPaneComponent;
  let fixture: ComponentFixture<SourcesPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourcesPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourcesPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
