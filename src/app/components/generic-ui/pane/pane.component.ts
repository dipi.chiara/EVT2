import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { ThemesService } from '../../../services/themes.service';

@Component({
    selector: 'evt-pane',
    templateUrl: './pane.component.html',
    styleUrls: ['./pane.component.scss']
})
export class PaneComponent implements OnInit {
    @Input() comparable: boolean;
    @Input() secondary: boolean;
    @Input() closable: boolean;
    @Input() hideHeader: boolean;
    @Input() hideFooter: boolean;
    @Input() showSecondaryContent: boolean;

    @Output() close = new EventEmitter<boolean>();

    constructor(public themes: ThemesService) {
    }

    ngOnInit() {
        this.comparable = this.comparable === undefined ? false : this.comparable;
        this.secondary = this.secondary === undefined ? false : this.secondary;
        this.closable = this.closable === undefined ? false : this.closable;
        this.hideHeader = this.hideHeader === undefined ? false : this.hideHeader;
        this.hideFooter = this.hideFooter === undefined ? false : this.hideFooter;
        this.showSecondaryContent = this.showSecondaryContent === undefined ? false : this.showSecondaryContent;
    }

    isSecondaryContentOpened(): boolean {
        return this.showSecondaryContent;
    }

    emitClose() {
        this.close.emit(true);
    }
}
