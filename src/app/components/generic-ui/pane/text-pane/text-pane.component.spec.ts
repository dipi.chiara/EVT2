import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextPaneComponent } from './text-pane.component';

describe('TextPaneComponent', () => {
  let component: TextPaneComponent;
  let fixture: ComponentFixture<TextPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
