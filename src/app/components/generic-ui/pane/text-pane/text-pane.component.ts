import { Component, Input } from '@angular/core';
import { PageData } from './../../../../models/evt-models';

@Component({
    selector: 'evt-text-pane',
    templateUrl: './text-pane.component.html',
    styleUrls: ['./text-pane.component.scss']
})
export class TextPaneComponent {
    @Input() page: PageData;
    // TEMP:
    public showSecondaryContent = false;
    public secondaryContent = '';

    isSecondaryContentOpened(): boolean {
        return this.showSecondaryContent;
    }

    toggleSecondaryContent(newContent: string) {
        if (this.secondaryContent !== newContent) {
            this.showSecondaryContent = true;
            this.secondaryContent = newContent;
        } else {
            this.showSecondaryContent = false;
            this.secondaryContent = '';
        }
    }

    getSecondaryContent(): string {
        return this.secondaryContent;
    }

}
