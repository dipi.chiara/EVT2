import { Component, Input } from '@angular/core';
import { PageData } from './../../../../models/evt-models';

@Component({
    selector: 'evt-image-pane',
    templateUrl: './image-pane.component.html',
    styleUrls: ['./image-pane.component.scss']
})
export class ImagePaneComponent {
    @Input() page: PageData;
}
