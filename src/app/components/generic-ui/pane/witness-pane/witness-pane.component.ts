import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PageData } from '../../../../models/evt-models';

@Component({
  selector: 'evt-witness-pane',
  templateUrl: './witness-pane.component.html',
  styleUrls: ['./witness-pane.component.scss']
})
export class WitnessPaneComponent {
  @Input() witness: string;
  @Input() page: PageData;
  @Output() close = new EventEmitter<boolean>();

  emitClose() {
    this.close.emit(true);
  }
}
