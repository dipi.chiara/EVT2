import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WitnessPaneComponent } from './witness-pane.component';

describe('WitnessPaneComponent', () => {
  let component: WitnessPaneComponent;
  let fixture: ComponentFixture<WitnessPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WitnessPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WitnessPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
