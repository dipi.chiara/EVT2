import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Language } from 'angular-l10n';
import { Router } from '@angular/router';

import { ConfigurationService } from '../../../configuration/configuration.service';
import { ThemesService } from '../../../services/themes.service';

import { EditionConfig } from '../../../configuration/evt-config';

@Component({
    selector: 'evt-main-header',
    templateUrl: './main-header.component.html',
    styleUrls: ['./main-header.component.scss']
})
export class MainHeaderComponent implements OnInit {
    @Language() lang: string;
    @Output() secondaryContentSelected = new EventEmitter<string>();

    editionConfig: EditionConfig;
    mainMenuOpened = false;

    constructor(
        private router: Router,
        public configuration: ConfigurationService,
        public themes: ThemesService) {
    }

    ngOnInit() {
        this.loadEditionConfig();
    }

    private loadEditionConfig(): void {
        this.configuration.getEditionConfig()
            .subscribe(editionConfig => this.editionConfig = editionConfig);
    }

    selectViewMode(viewMode: string) {
        this.router.routerState.root.firstChild.params
            .subscribe((params) => {
                this.router.navigate(['/' + viewMode, params]);
            });
    }

    toggleMainMenu(event) {
        this.mainMenuOpened = !this.mainMenuOpened;
        if (event !== undefined) {
            this.secondaryContentSelected.emit(event);
        }
    }
}
