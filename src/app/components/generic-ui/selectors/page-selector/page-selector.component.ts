import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { PageData, EditionStructure } from '../../../../models/evt-models';
import { StructureXmlParserService } from '../../../../services/xml-parsers/structure-xml-parser.service';

@Component({
  selector: 'evt-page-selector',
  templateUrl: './page-selector.component.html',
  styleUrls: ['./page-selector.component.scss']
})
export class PageSelectorComponent implements OnInit {
  @Input() page: PageData;
  public editionStructure: EditionStructure;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private structureXML: StructureXmlParserService) {
  }

  ngOnInit() {
    this.initPagesList();
  }

  selectPage(pageId: string) {
    const viewMode = this.route.snapshot.routeConfig.path;
    const params = JSON.parse(JSON.stringify(this.route.snapshot.params));
    params.page = pageId;
    this.router.navigate(['/' + viewMode, params]);
  }

  private initPagesList() {
    this.structureXML.getStructure().subscribe((editionStructure: EditionStructure) => {
      this.editionStructure = editionStructure;
    });
  }
}
