
import { Component, OnInit, Output, EventEmitter, HostListener } from '@angular/core';
import { LocaleService, TranslationService, Language } from 'angular-l10n';

import { ConfigurationService } from '../../../configuration/configuration.service';
import { ThemesService, ColorTheme } from '../../../services/themes.service';

import { UiConfig } from '../../../configuration/evt-config';

@Component({
  selector: 'evt-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {
    @Language() lang: string;
    @Output() itemClicked = new EventEmitter<string>();
    public uiConfig: UiConfig;
    private isOpened = false;

    constructor(
        public locale: LocaleService,
        public translation: TranslationService,
        public configuration: ConfigurationService,
        public themes: ThemesService) {
    }

    ngOnInit() {
        this.loadUiConfig();
        this.isOpened = true;
    }

    @HostListener('click')
    clickInside() {
        this.isOpened = true;
    }

    @HostListener('document:click')
    clickout() {
        if (!this.isOpened) {
            this.itemClicked.emit('close');
        }
        this.isOpened = false;
    }

    private loadUiConfig(): void {
        this.configuration.getUiConfig()
            .subscribe(uiConfig => this.uiConfig = uiConfig);
    }

    openGlobalDialogInfo() {
        // TODO openGlobalDialogInfo
        console.log('openGlobalDialogInfo');
        this.itemClicked.emit('globalInfo');
    }
    openGlobalDialogLists() {
        // TODO openGlobalDialogLists
        console.log('openGlobalDialogLists');
        this.itemClicked.emit('lists');
    }
    generateBookmark() {
        // TODO generateBookmark
        this.itemClicked.emit('bookmark');
    }
    downloadXML() {
        // TODO downloadXML
        console.log('downloadXML');
        this.itemClicked.emit('downloadXML');
    }
    openShortCuts() {
        // TODO openShortCuts
        this.itemClicked.emit('shortcuts');
    }

    selectLanguage(language: string): void {
        this.locale.setCurrentLanguage(language);
        this.itemClicked.emit('language');
    }

    selectTheme(theme: ColorTheme) {
        this.themes.selectTheme(theme);
        this.itemClicked.emit('theme');
    }
}
