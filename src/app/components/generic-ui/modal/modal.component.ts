import { Component, OnInit, Input, Output, EventEmitter, ElementRef, HostListener, ViewChild } from '@angular/core';

import { ThemesService } from '../../../services/themes.service';

@Component({
    selector: 'evt-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
    @Input() modalId: string;
    @Input() closeOnShadow: boolean;
    @Input() closeOnEsc: boolean;
    @Input() fixedHeight: boolean;
    @Input() header: any;
    @Input() body: any;
    @Input() footer: any;

    @Output() close = new EventEmitter<string>();

    @ViewChild('modalDialog') modalDialog: ElementRef;

    constructor(public themes: ThemesService) {
    }

    ngOnInit() {
        this.closeOnShadow = this.closeOnShadow === undefined ? true : this.closeOnShadow;
        this.closeOnEsc = this.closeOnEsc === undefined ? true : this.closeOnEsc;
        this.fixedHeight = this.fixedHeight === undefined ? false : this.fixedHeight;
    }

    @HostListener('click', ['$event'])
    clickout(event) {
        const modal = this.modalDialog.nativeElement;
        const internalClick: boolean = event.path.find(function(o) {
            return o.className && o.className.indexOf && o.className.indexOf(modal.className) >= 0; });
        if (this.closeOnShadow && !internalClick) {
            this.closeDialog();
        }
    }

    @HostListener('window:keyup', ['$event'])
    keyEvent(event: KeyboardEvent) {
        if (this.closeOnEsc && event.keyCode === 27) {
            this.closeDialog();
        }
    }

    closeDialog() {
        this.close.emit(this.modalId);
    }
}
