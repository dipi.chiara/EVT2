import { Map } from '../utils/jsUtils';

// ////////////// //
// NAMED ENTITIES //
// ////////////// //
export interface PersName {
    forename: string;
    surname?: string;
}

export interface Person {
    id: string;
    persName: PersName;

    role?: Element[];
    affiliation?: Element[];
    age?: Element[];
    birth?: Element[];
    death?: Element[];
    education?: Element[];
    event?: Element[];
    faith?: Element[];
    floruit?: Element[];
    langKnowledge?: Element[];
    nationality?: Element[];
    occupation?: Element[];
    person?: Element[];
    residence?: Element[];
    sex?: Element[];
    socecStatus?: Element[];
    state?: Element[];
    trait?: Element[];

    xml: string;
    relations?: string[];
}

export interface Relation {
    type: string;
    name: string;
    active: string[];
    passive: string[];
    mutual: string[];
}
export interface ListPerson {
    id: string;
    persons: Map<Person>;
    relations: Map<Relation>;
}

export interface Place {
    id: string;
    xml: string;

    placeName?: Element[];
    place?: Element[];
    bloc?: Element[];
    climate?: Element[];
    country?: Element[];
    district?: Element[];
    event?: Element[];
    geogName?: Element[];
    location?: Element[];
    population?: Element[];
    region?: Element[];
    settlement?: Element[];
    state?: Element[];
    terrain?: Element[];
    trait?: Element[];
    // listEvent: string;
    // listPlace: string;
}

export interface ListPlace {
    id: string;
    places: Map<Place>;
}
// ////////////////// //
// END NAMED ENTITIES //
// ////////////////// //
