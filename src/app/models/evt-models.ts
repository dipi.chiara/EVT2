import { Map } from '../utils/jsUtils';

export interface EditionStructure {
    pages: Map<PageData>;
    pagesIndexes: string[];
}

export interface PageData {
    id: string;
    label: string;
    xmlSource: any;
    content: any[];
}

export interface NamedEntity {
    header: string;
    xml: string;
    occurrences: string[];
    info: { icon: string, text: string, label?: string }[];
    type: string;
}
