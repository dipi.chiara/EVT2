export interface GenericElementData {
    type: any;
    class: string;
    content: any;
}

export interface TextData {
    type: any;
    text: string;
}

export interface NoteData {
    type: any;
    path: string;
    content: any;
}

export interface NamedEntityRefData {
    type: any;
    path: string;
    entityType: string;
    content: any;
    entityId: string;
}

export interface CommentData {
    type: 'comment';
}
